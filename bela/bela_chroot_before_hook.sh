#!/bin/sh

# copy them over to /etc/*
#sudo mkdir -p "${tempdir}/etc/default"
sudo cp -v "${DIR}/bela/bela-customizations/isc-dhcp-server" "${tempdir}/etc/default/isc-dhcp-server"
sudo chown root:root "${tempdir}/etc/default/isc-dhcp-server" || true

sudo cp -v "${DIR}/bela/bela-customizations/dhcpd.conf" "${tempdir}/etc/dhcp/dhcpd.conf"
sudo chown root:root "${tempdir}/etc/dhcp/dhcpd.conf" || true

sudo cp -v "${DIR}/bela/bela-customizations/modules.conf" "${tempdir}/etc/modules-load.d/modules.conf"
sudo chown root:root "${tempdir}/etc/modules-load.d/modules.conf" || true

sudo cp -v "${DIR}/bela/bela-customizations/interfaces" "${tempdir}/etc/network/interfaces"
sudo chown root:root "${tempdir}/etc/network/interfaces" || true

sudo cp -v "${DIR}/bela/bela-customizations/sshd_config" "${tempdir}/etc/ssh/sshd_config"
sudo chown root:root "${tempdir}/etc/ssh/sshd_config" || true

sudo cp -v "${DIR}/bela/bela-customizations/bela" "${tempdir}/etc/sudoers.d/bela"
sudo chown root:root "${tempdir}/etc/sudoers.d/bela" || true

sudo cp -v "${DIR}/bela/bela-customizations/wpa_supplicant.conf" "${tempdir}/etc/wpa_supplicant/wpa_supplicant.conf"
sudo chown root:root "${tempdir}/etc/wpa_supplicant/wpa_supplicant.conf" || true

sudo cp -v "${DIR}/bela/bela-customizations/fstab" "${tempdir}/etc/fstab"
sudo chown root:root "${tempdir}/etc/fstab" || true

sudo cp -v "${DIR}/bela/bela-customizations/hosts" "${tempdir}/etc/hosts"
sudo chown root:root "${tempdir}/etc/hosts" || true

sudo cp -v "${DIR}/bela/bela-customizations/motd" "${tempdir}/etc/motd"
sudo chown root:root "${tempdir}/etc/motd" || true

sudo cp -v "${DIR}/bela/bela-customizations/securetty" "${tempdir}/etc/securetty"
sudo chown root:root "${tempdir}/etc/securetty" || true

# copy them over to /opt/Bela
sudo mkdir -p "${tempdir}/opt/Bela"
sudo cp -v "${DIR}/bela/bela-customizations/bela_bootloader.sh" "${tempdir}/opt/Bela/bela_bootloader.sh"
sudo cp -v "${DIR}/bela/bela-customizations/bela_button_hold.sh" "${tempdir}/opt/Bela/bela_button_hold.sh"
sudo cp -v "${DIR}/bela/bela-customizations/bela_flash_emmc.sh" "${tempdir}/opt/Bela/bela_flash_emmc.sh"
sudo cp -v "${DIR}/bela/bela-customizations/bela_gadget.sh" "${tempdir}/opt/Bela/bela_gadget.sh"
sudo cp -v "${DIR}/bela/bela-customizations/bela_mac.sh" "${tempdir}/opt/Bela/bela_mac.sh"
sudo cp -v "${DIR}/bela/bela-customizations/bela_startup.sh" "${tempdir}/opt/Bela/bela_startup.sh"
sudo chown root:root "${tempdir}/opt/Bela/*" || true

# copy them over to /lib/systemd/system
sudo cp -v "${DIR}/bela/bela-customizations/bela_button.service" "${tempdir}/lib/systemd/system/bela_button.service"
sudo cp -v "${DIR}/bela/bela-customizations/bela_flash_emmc.service" "${tempdir}/lib/systemd/system/bela_flash_emmc.service"
sudo cp -v "${DIR}/bela/bela-customizations/bela_gadget.service" "${tempdir}/lib/systemd/system/bela_gadget.service"
sudo cp -v "${DIR}/bela/bela-customizations/bela_ide.service" "${tempdir}/lib/systemd/system/bela_ide.service"
sudo cp -v "${DIR}/bela/bela-customizations/bela_shutdown.service" "${tempdir}/lib/systemd/system/bela_shutdown.service"
sudo cp -v "${DIR}/bela/bela-customizations/bela_startup.service" "${tempdir}/lib/systemd/system/bela_startup.service"
sudo cp -v "${DIR}/bela/bela-customizations/dhclient_shutdown.service" "${tempdir}/lib/systemd/system/dhclient_shutdown.service"
sudo cp -v "${DIR}/bela/bela-customizations/ssh_shutdown.service" "${tempdir}/lib/systemd/system/ssh_shutdown.service"
sudo chown root:root "${tempdir}/lib/systemd/system/*" || true 

# copy them over to /root
sudo cp -v "${DIR}/bela/bela-customizations/.bashrc" "${tempdir}/root/.bashrc"
sudo cp -v "${DIR}/bela/bela-customizations/.gdbinit" "${tempdir}/root/.gdbinit"
sudo cp -v "${DIR}/bela/bela-customizations/.gitconfig" "${tempdir}/root/.gitconfig"
sudo cp -v "${DIR}/bela/bela-customizations/.vimrc" "${tempdir}/root/.vimrc"
sudo chown root:root "${tempdir}/root/*" || true 



